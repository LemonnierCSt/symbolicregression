#include "tree.h"
#include <iostream>
#include <list>

using std::cout;
using std::endl;

int Tree::data_length_ = 3; //initialisation



Tree::Tree()//Constructeur
{
  first_node_ = new Node(1,0,rand()%data_length_,nullptr);
  length_=1;
}

Tree::~Tree()
{
  delete first_node_;
}


Tree::Tree(const Tree &model){//Constructeur par copie
  length_ = model.length_;
  Node* tmp[model.length_]; //tmp est le tableau de noeuds 
  tmp[0]=model.first_node_;//on met le premier noeud  dedans
  int position=1;
  length_=model.first_node_->parcours(tmp,&position);//remplit le tableau tmp avec tous les noeuds de l'arbre "modèle" dans l'ordre 
  
  Node* current; 
  Node* to_copy[length_]; // tableau destiné a contenir ls adresses des noeuds du nouvel arbre qui seront mis dans de futurs next_nodes. et effacés de ce tableau.



  for (int i=length_-1; i>=0; i--){//cette boucle parcourt la liste des noeuds à copier
    current=tmp[i];//noeud à copier
    Node* new_n=new Node();//nouveau noeud vierge
    if (new_n ->type_ ==0) // supprimer tableau si operateur
    {
      delete[] (new_n ->next_nodes_);
    }
    to_copy[i] = new_n;//on met son adresse dans le tableau des adresses des noeuds copiés
    new_n->type_= current->type_;//on set ses attributs
    new_n->value_ = current->value_;
    new_n->nbr_next_nodes_=current->nbr_next_nodes_;
    
    int nb_nullptr = 0;    
    if (current->type_==0)//0 = type opérateur
    {
    new_n -> next_nodes_ = new Node*[new_n->nbr_next_nodes_+1];// On crée le pointeur sur son attribut next_nodes_.
      for(int j = 0; j< current->nbr_next_nodes_; j++)
      {// On boucle pour savoir où se situent les next_nodes_ du "current node" à ajouter à notre noeud
        while (to_copy[1+i+nb_nullptr] == nullptr)
        { // Chaque case déjà utilisée (copiée vers next_nodes_ ) contient maintenant "nullptr" dans le tableau "to_copy"
          ++nb_nullptr;//compte le nbr de nullptr
        }
        new_n->next_nodes_[j] = to_copy[i+1+nb_nullptr];
        to_copy[i+1+nb_nullptr] = nullptr;
        nb_nullptr = 0;
      }
    }
    if(i==0){
        first_node_=new_n;//Set l'attribut first_node de la copie crée
    }
  }
}


Tree::Tree(Node* first_node, int length)//Constructeur par passage de parametre,

{
  length_ = length; // Nombre total de noeuds dans l'arbre
  first_node_ = first_node;
}

//####################### Getter ###########################

Node* Tree::get_first() const
{
  return first_node_;
}

//#######################Method###########################
Node* Tree::access_node(int position)
/*
  Prend en parametre une position
  Appelle la fonction parcours
  Renvoie le noeud correspondant à la position
*/
{
  Node* node[length_];          //Déclare un tableau d'adresse de noeud de taille équivalent a la taille de l'arbre
  node[0]=first_node_;          //Met en case 0 le premier noeud de l'arbre
  int depart=1;                 //Met le point de départ à 0
  first_node_->parcours(node,&depart);  //Appelle la fonction parcours de noeud
  return node[position];      
}

void Tree::affichage_tree()
{
  Node* node[length_];
  node[0]=first_node_;
  int depart=1;
  first_node_->parcours(node,&depart);
  cout<<"----- TREE ------"<<endl;
  for(int i=0;i<depart;i++)
  {
    cout<<"----- NODE ------"<<endl;
    node[i]->display();
    cout<<"-----------"<<endl;
  }
}


void Tree::deletion()
//Supprime un noeud operateur et le remplace par une noeud terminal
{
  if(first_node_->type_==0)                     //Si le premier noeud est un opérateur, on peut appliquer au moins une déletion
  {
    Node* node = access_node(rand()%length_);   //Recupere un noeud aléatoire de l'arbre
    while(node->type_!=0)                       //Verifie que le noeud récupere est un opérateur si ce n'est pas le cas en génère un nouveau
    {
      node=access_node(rand()%length_);
    }
    Node* tab_node[length_];
    if(node==first_node_)                        //Si le noeud à suprimer est le premier
    {
      first_node_->modif_values(rand()%2+1);
      length_=1;
    }
    else
    {
      node->modif_values(rand()%2+1);                    //Génére un noeud terminal pour remplacer l'opérateur
      int depart=1;
      tab_node[0]=first_node_;
      length_=first_node_->parcours(tab_node,&depart);        //Recalcul la taille de l'arbre
    }
  }
}


void Tree::addition()
//Modifie un noeud terminal en un noeud operateur
{
  Node* node = access_node(rand()%length_);               //Accede à un noeud aléatoire
  while(node->type_==0)                                   //Vérifie que le noeud est bien un noeud terminal
  {
    node=access_node(rand()%length_);                     //Si ce n'est pas le cas regénère un noeud
  }
  if(node!=0)
  {
    node->modif_values(0);                              //Modifie la valeur terminal et opérateur
    for(int i=0;i<node->nbr_next_nodes_;i++)
    {
      node->next_nodes_[i]=new Node(1,0,rand()%2,nullptr);   //Génère x noeud terminaux apres le noeud opérateur
    }
    length_=length_+node->nbr_next_nodes_;                  //Modifie la taille
  }
}


void Tree::mutation()
//Modifie un noeud terminal en un noeud terminal ou un noeud operateur en un noeud operateur
{
  Node* node = access_node(rand()%length_);             //Accede à un noeud aléatoire
  Node* new_node=new Node;                              //Génère un noeud
  new_node->modif_values(node->type_);                  //Le modifie pour avoir un noeud de meme type que celui tiré aléatoire
  if(new_node->nbr_next_nodes_ == node->nbr_next_nodes_)//Si les opérateurs on le meme nombre de noeud consécutif (AND -> OR par exemple)
  {
    node->value_=new_node->value_;                      //Modifie seulement la valeur du noeud
  }
  else if(new_node->nbr_next_nodes_ > node->nbr_next_nodes_)  //Si le nouvel opérateur attend plus de noeud successif (NOT -> AND par exemple)
  {
    node->value_=new_node->value_; 
    Node** tmp = (Node**) malloc (new_node->nbr_next_nodes_*sizeof(Node*)); //Déclare un nouveau tableau
    for(int i=0;i<(node->nbr_next_nodes_);i++)                              //Copie les valeur du noeud dans tmp
    {
      tmp[i]=node->next_nodes_[i];
    }
    for(int i=(node->nbr_next_nodes_);i<(new_node->nbr_next_nodes_);i++)    //Ajout des noeud terminaux pour les valeurs manquantes
    {
      tmp[i]=new Node(1,0,(rand()%2+1),nullptr);
    } 
    length_=length_+(new_node->nbr_next_nodes_)-(node->nbr_next_nodes_);     //Modifie la taille
    node->nbr_next_nodes_=new_node->nbr_next_nodes_;
    node->next_nodes_=tmp;
  }
  else                                                         //Si le nouvel opérateur attend moins de noeud successif (AND -> NOT par exemple)
  {
    node->value_=new_node->value_;
    Node** tmp = new Node*[new_node->nbr_next_nodes_]; //Déclare un nouveau tableau
    for(int i=0;i<new_node->nbr_next_nodes_;i++)                              //Copie les x premieres valeurs nécéssaires du noeud dans tmp
    {
      tmp[i]=node->next_nodes_[i];
    }
    Node *tab_node[length_];
    tab_node[0]=first_node_;
    int depart=1;
    length_=first_node_->parcours(tab_node,&depart);                      //Recalcul la taille
    node->nbr_next_nodes_=new_node->nbr_next_nodes_;
    for(int i=new_node->nbr_next_nodes_;i<node->nbr_next_nodes_;i++)      //Supprime la partie de l'arbre non prise en compte
    {
      free(node->next_nodes_[i]);
    }
    node->next_nodes_=tmp;
  }
}



float Tree::get_value(const float data[]) // return the value of a fonction given the following data

{
  return first_node_->compute(data);
}


std::string Tree::get_expression()
{
  return first_node_ ->get_expression();
}





