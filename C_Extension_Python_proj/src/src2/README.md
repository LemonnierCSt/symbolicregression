tree :: get_value. Retourne la valeur prédite avec un arbre donné pour une valeur de X. data[] représente un vecteur de valeur correspondant aux différents xi.
tree :: get_expression. Retourne l'expression sous forme d'un string.
tree :: access_node. Accède au pointeur sur le noeud désiré. La numérotation s'effectue en parcourant les branches une par une. 

individual :: get_fitness. Retourne la fitness d'un individu.
individual :: calcul_fitness.

node :: conversion_[] : tableau de conversion d'un float vers une opération. 
node :: value_ : correspond à la valeur du noeud (à convertir si besoin en opérateur).
node :: operation_result(X) : fait l'operation à partir des variables.


utilisation de wrapper (python utilisant librairie de c++) : import mywrapper
  - if (!PyArg_ParseTuple(args,"hO",&a,&obj)) : prend un tuple en entrée (un entier et un objet). 
  - créer une capsule (pointeur c++ sur objet c++ , nom capsule, destrcteur) : crée un objet python. 
  - ne sert qu'à donner accès à certaines méthodes à l'utilisateur. 
