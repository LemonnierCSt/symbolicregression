#include "individual.h"
#include "tree.h"
#include <iostream>
#include "tree.h"
#include "node.h"


int Individual::data_length_ = 3; //initialisation

// Setters

void set_data_length(int data_length)
{
  Individual::data_length_ = data_length;
  Node::data_length_ = data_length;
  Tree::data_length_ = data_length;
}


  // Constructors

Individual::Individual()
{
  Tree::data_length_ = Individual::data_length_; // we have to do this one time only
  tree_ = new Tree;
  fitness_ = 0.0;
  generation_=0;
}


Individual::Individual(const Individual& mother) //daugther constructor
{
  Tree* tree= new Tree(*mother.tree_);
  tree_= tree;
  fitness_ = mother.fitness_;
  generation_=mother.generation_+1;

}


Individual::Individual(Tree& tree,float fitness){//Constructor  by parameter
  tree_= &tree;
  fitness_=fitness;
}



  // Methods
void Individual::fitness_compute(float * const data[],
                                  const float observations[], int nbr_observations) // compute fitness with the given data
{
  int R0 = 0;
  for (int i=0 ; i<nbr_observations; ++i)
  {
    float ecart =  (tree_ -> get_value(data[i])) - observations[i] ;
    R0 += ecart*ecart;
  }
  fitness_ = R0;
}

Individual* demo() //Dans cette fonction, il faut créér un individu avec un arbre qui correspond à la fonction (X0 AND X1) OR X2, et une fitness de 0.833
{
  Node* noeud11=new Node(0,0,1,nullptr);
  Node* noeud12=new Node(0,0,0,nullptr);
  Node *tab[] = {noeud11};
  Node* noeud21=new Node(1,1,2,tab);
  Node *tab2[] = {noeud12,noeud21};
  Node* noeud2=new Node(1,2,0,tab2);

  Tree arbre(noeud2,3);
  Individual* tmp=new Individual(arbre, 0.833);

  //float data[5][3] = {{0,1,0},{0,1,1},{0,0,1},{1,0,0},{1,1,0}};   //the vectors of data X0 {0,1,0} X1{0,1,1} X2{0,0,1} X3{1,0,0} X4{1,1,0}
  //float observations[5] = {0,1,1,0,1};// The vector of observed values used was Y(0,1,1,0,1)

return tmp;
}


void Individual::View(){
  std::cout<<"view"<<std::endl;
}




void Individual::generate_daugther(int nbr_daugther,Individual *tab[])
{
  for(int i=0;i<nbr_daugther;i++)
  {
    Individual* tmp=new Individual(*this);
    tab[i]=tmp;
  }
  for(int i=0;i<nbr_daugther;i++)
  {
    tab[i]->modif_individual();
  }

}

void Individual::modif_individual()
{
  int modification=rand()%3+1;
  switch(modification)
  {
    case 1: tree_->addition();
        break;
    case 2: tree_->deletion();
        break;
    case 3: tree_->mutation();
        break;
  }
}

Individual* best_individual(Individual** tab, int nbr_daughters, Individual* mother)
{
  Individual* best=mother;
  for (int i=0; i<nbr_daughters; i++)
  {
    if (tab[i]->get_fitness() <= best -> get_fitness())
    {
      delete best; // Oedipe syndrom
      best = tab[i];
    }
    else
    {
      delete tab[i];
      tab[i]=nullptr; // The weaks must die
    }
  }
//  delete[] tab;
  return best;
}

Individual* evolution_root(float**  data,int data_length,  float observations[], int nbr_observations, int nbr_iterations, int nbr_daugthers, float fitness_tab[]) 
{

  set_data_length(data_length) ;
  Individual* Eve=new Individual; // the first of the world
  for(int i=0;i<5;i++)
  {
    Eve->get_tree()->addition();
  }
  Eve->fitness_compute(data,observations,nbr_observations);
  fitness_tab[0]=Eve->get_fitness();
  Individual* best = Eve; // being alone has some benefits : technically you're always the best
  int i=0;
  int generation =0;
  Individual* family[nbr_daugthers];
  while ( (generation<nbr_iterations)&&(best->get_fitness()!=0) )
  {
    best -> generate_daugther(nbr_daugthers,family); 
    for (i =0 ; i<nbr_daugthers ; ++i)
    {
      family[i] ->fitness_compute(data,observations,nbr_observations);
    }
    best=best_individual(family,nbr_daugthers,best); //Only the strongest will prevail
    fitness_tab[generation+1]=best->get_fitness();
    generation+=1;
  }
  return best;
}


