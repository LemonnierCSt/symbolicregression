#ifndef NODE_H
#define NODE_H
#include <string>

class Node
{
  protected:

    static std::string conversion_[];
    int type_; // 0=operator 1=variable 2=constant
    int nbr_next_nodes_;
    float value_;
    Node** next_nodes_;
    static int data_length_;


  public:

    //Constructors
    Node();
    Node(int type, int nbr_next_nodes, float value, Node** next_nodes);
    Node(const Node &node)=delete;

    ~Node();

    //Getters

    int get_type();
    int get_nbr_next_nodes();
    float get_value();
    Node* get_next_node(int position);
    inline int data_length()const;

    //Methods

    std::string get_expression();
    void display();
    float compute(const float data[]); // compute the result : recursive method that returns the value of the tree
    float operation_result(float X); // currently for "not" operators
    float operation_result(float X, float Y); // currently for "and" and "or" operator
    int parcours(Node* tab_node[],int* position); // Return the position of a desired node. To go through, the order is one branch by one.
    void modif_values(int type); // Modifies attributs of a node.
    
  private:

    friend class Tree;
    friend void set_data_length(int data_length);
};

int Node::data_length()const
{
  return Node::data_length_;
}

#endif // NODE_H

