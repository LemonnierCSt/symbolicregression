#include "node.h"
#include <iostream>
using std::cout;
using std::endl;

std::string Node::conversion_[]= {"AND","OR","NOT"};
int Node::data_length_ = 3; //initialisation

Node::Node()
{
    type_=rand()%3;

    if (type_==0) //operator
    {
      value_=rand()%3; //only for booleans
      if (value_ ==2) //Not
      {
        nbr_next_nodes_=1;
      }
      else // and / or
      {
        nbr_next_nodes_=2;
      }
      next_nodes_=new Node*[nbr_next_nodes_];
      
      for (int i=0; i<nbr_next_nodes_;i++)
      {
        next_nodes_[i]= nullptr;
      }
    }
    else if (type_==1) //variable
    {
      nbr_next_nodes_=0;
      value_=rand()%data_length_;
      next_nodes_=nullptr; 
    }
    else //constant
    {
      nbr_next_nodes_=0;
      value_=rand()%2;
      next_nodes_=nullptr;
    }
}

Node::Node(int type, int nbr_next_nodes, float value, Node* next_nodes[]){
  type_= type;
  value_ = value;
  nbr_next_nodes_=nbr_next_nodes;
  next_nodes_ = new Node* [nbr_next_nodes_];

  for (int i=0;i<nbr_next_nodes_;i++)
  {
    next_nodes_[i]= next_nodes[i];
  }
}




Node::~Node()
{
  if (next_nodes_!=nullptr)
  {
    /*for(int i=0;i<nbr_next_nodes_;i++)
    {
      delete next_nodes_[i];
    }*/
    delete[] next_nodes_;
  }
}




int Node::get_type()
{
  return type_;
}

int Node::get_nbr_next_nodes()
{
  return nbr_next_nodes_;
}

float Node::get_value()
{
  return value_;
}

Node* Node::get_next_node(int position)
{
  if (type_ != 0){
    return nullptr;
  }
  else {
    return next_nodes_[position];
  }
}

void Node::display()
{
  cout<< "Type :" << type_<<endl;
  if (type_==0){
    cout<< "Which operator :" << conversion_[int(value_)]<<endl;
  }
  else
  {
    cout<< "Value :" << value_<<endl;
  }
  for (int i=0;i<nbr_next_nodes_;i++)
  {
    cout<< "Adresse " << next_nodes_[i]<<endl;
  }
}

float Node::compute(const float data[])
{
    if (type_==0) //operator
    {
      if (nbr_next_nodes_==1){
        return (operation_result(next_nodes_[0]->compute(data)));
      }
      else if (nbr_next_nodes_==2){
        return (operation_result(next_nodes_[0]->compute(data),next_nodes_[1]->compute(data)));
      }
    }
    else if (type_==2) //constant
    {
      return value_;
    }   
    else //data
    {
      return data[static_cast<int>(this->value_)]; // return the data at the given position
    }
    return 0;
}

float Node::operation_result(float X, float Y)
{
  if (value_ == 0)
  {
    return X && Y;
  }
  if (value_ == 1)
  {
    return X || Y;
  }
  else
  {
    cout << "error" << endl;
    return 0 ;
  }
}


float Node::operation_result(float X){
  if (value_ == 2)
  {
    return not(X);
  }
  else
  {
    cout << "error" << endl;
    return 0 ;
  }
}

int Node::parcours(Node* tab_node[],int* position)
/*
Fonction recursive qui ajoute dans un tableau les noeuds a la position 0 et qui renvoie le nombre de noeud sous le noeud actuelle
*/
{
  int size=0;       //Nombre noeuds après le noeud actuel( noeud actuel inclus)
  if(type_==0)      //Test si le noeud est un opérateur
  {
    for(int i=0;i<nbr_next_nodes_;i++)
    {
      tab_node[*position]=next_nodes_[i];                 //Ajout le noeud a la position suivante
      *position+=1;
      size+=next_nodes_[i]->parcours(tab_node,position);  //Rappelle la fonction
    }
    return size+=1;   //Renvoie la taille +1
  }
  else
  {
    return 1;//il n'y a qu'un noeud( celui actuel);
  }
}




void Node::modif_values(int type)
{
//  delete[] next_nodes_;
  type_=type;
  //delete[] next_nodes_;
  if (type_==0)  
  {
    value_=rand()%3;
    if (value_ ==2)
    {
      nbr_next_nodes_=1;
    }
    else
    {
      nbr_next_nodes_=2;
    }
    next_nodes_ = new Node* [nbr_next_nodes_];
    for (int i=0; i<nbr_next_nodes_;i++)
    {
      next_nodes_[i]= nullptr;
    }
  }
  else
  {
      if(type==1)
      {
          value_=rand()%data_length_;
      }
      else
      {
          value_=rand()%2;
      }
    nbr_next_nodes_=0;
    next_nodes_= nullptr; 
  }
}

std::string Node::get_expression(){
  std::string formula;
  if (this -> type_ == 0){
    if (this -> nbr_next_nodes_ == 2){ // "AND" and "OR"
      formula += "(";
      formula += next_nodes_[0] -> get_expression();
      formula += " ";
      formula += this -> conversion_[(int) this -> value_];
      formula += " ";
      formula += next_nodes_[1] -> get_expression();
      formula += ")";
    }
    else if (this -> nbr_next_nodes_ == 1){ // "NOT"
      formula += "(";
      formula += this -> conversion_[(int) this -> value_];
      formula += "(";
      formula += next_nodes_[0] -> get_expression();
      formula += ")";
      formula += ")";
    }
  }
  else if (this->type_ == 2){ // "CONSTANT"
    formula += std::to_string(this -> value_);
  }
  else { // "VARIABLE"
    formula += "X";
    formula += std::to_string(static_cast<int>(this->value_));
  }
  return formula;
}

