#ifndef INDIVIDUAL_H
#define INDIVIDUAL_H

#include "tree.h"
#include <string>


class Individual
{
  protected:
    float fitness_;
    Tree* tree_;
    static int data_length_;
    int generation_;

  public:
    //Constructors
    Individual(const Individual& mother); //daugther constructor
    Individual();
    Individual( Tree& tree,float fitness);

    //Getters
    inline float get_fitness()const;
    inline int data_length()const;
    inline Tree* get_tree()const;
    inline int get_generation()const;


    //Methods
    void fitness_compute( float * const data[],
                          const float observations[], int nbr_observations); // compute fitness with the given data
    std::string get_expression()const;
    void View();
    void generate_daugther(int nbr_daugther, Individual* tab[]);
    void modif_individual();


  private :
    friend void set_data_length(int data_length);

};

float Individual::get_fitness()const
{
  return fitness_ ;
}

int Individual::data_length()const
{
  return Individual::data_length_;
}

inline Tree* Individual::get_tree()const
{
  return tree_;
}
inline int Individual::get_generation()const
{
  return generation_;
}



Individual* demo();

void set_data_length(int data_length);

Individual *best_individual(Individual** tab, int nbr_daughters, Individual *mother);

Individual *evolution_root(float **  data, int data_length, float observations[], int nbr_observations, int nbr_iterations, int nbr_daugthers, float fitness_tab[]); // No mercy for the weakest

#endif // INDIVIDUAL_H
