#ifndef Tree_H
#define Tree_H
#include <string>
#include "node.h"

class Tree
{
  protected :

    int length_;
    Node* first_node_;
    static int data_length_;

  public:

    // Constructors

    Tree();
    Tree(const Tree &model);//copy constructor
    Tree(Node* first_node, int length);// create a tree for tests and gives the number of nodes


    // Destructor
    ~Tree();

    // Getters
    Node* get_first()const;                      //Renvoie le premier noeud
    inline int get_length()const;                //Renvoie la longueur de l'arbre
    inline int data_length()const;               //Renvoie la data_length de l'arbre


    // Setters
    


    // Mutations
    void deletion();                             //Supprime un noeud operateur et le remplace par une noeud terminal
    void addition();                             //Modifie un noeud terminal en un noeud operateur
    void mutation();                             //Modifie un noeud terminal en un noeud terminal ou un noeud operateur en un noeud operateur

    // Conversion
    std::string get_expression(); // translate the tree to string
    float get_value(const float data[]); // evaluate the value of the expression with a given variable

   // Methods
    Node* access_node(int position);            //Accede au noeud situé à la position donnée
    void affichage_tree();                      //Affiche l'arbre

  private:
    friend class Individual;
    friend void set_data_length(int data_length);

};

int Tree::get_length()const
{
  return length_ ;
}

int Tree::data_length()const
{
  return Tree::data_length_;
}

#endif // Tree_H
