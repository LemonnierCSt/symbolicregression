#include <iostream>
#include <stdlib.h>
#include <string>

#include "../individual.h"
#include "../node.h"
#include "../tree.h"

#include "function_test_individual.h"
#include "function_test_tree.h"
#include "function_test_node.h"

using std::cout;
using std::endl;
using std::string;



int main()
{
  srand(time(NULL));
  test_individual_default_constructor();
  test_individual_copy_constructor();
  test_individual_data_length();
  test_individual_value_constructor();
  test_individual_fitness_compute();
  test_individual_modif_individual();
  test_individual_generate_daugther();
  Individual* myindividual2= demo();
  test_individual_best_individual();
  test_evolution_root();
  return 0;
}
