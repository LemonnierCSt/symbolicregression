#include <iostream>
#include <stdlib.h>
#include <string>

#include "../individual.h"
#include "../node.h"
#include "../tree.h"

#include "function_test_individual.h"
#include "function_test_tree.h"
#include "function_test_node.h"

using std::cout;
using std::endl;
using std::string;

void test_individual_data_length()
{
  test_display("data_length_individual");

  Individual myindividual;
  Individual myindividual2;
  cout<<(myindividual.data_length()==3)<<endl;
  cout<<(myindividual2.data_length()==3)<<endl;

  test_tree_data_length();
  test_node_data_length();

  set_data_length(5);

  test_tree_data_length(5);
  test_node_data_length(5);

  test_display("data_length_individual");


  Individual myindividual3;
  Individual myindividual4;
  cout<<(myindividual.data_length()==5)<<endl;
  cout<<(myindividual2.data_length()==5)<<endl;

}

void test_individual_value_constructor()
{
  test_display("individual_value_constructor");

  Node* myvalue=new Node(1,0,2,nullptr);
  Node* myconstant=new Node(2,0,0,nullptr);
  Node* mytab[1];
  mytab[0]= myvalue;
  Node* mynode1=new Node(0,1,2,mytab);
  Node* mytab2[2] ;
  mytab2[0]=myconstant;
  mytab2[1]=mynode1;
  Node* mynode2=new Node(0,2,1,mytab2);

  Tree mytree1(myvalue,1);
  Tree mytree2(myconstant,1);
  Tree mytree3(mynode1,2);
  Tree mytree4(mynode2,4);

  Individual myman1(mytree1, 1);
  Individual myman2(mytree2, 2);
  Individual myman3(mytree3, 3);
  Individual myman4(mytree4, 4);

  assert(myman1.get_fitness()==1);
  assert(myman2.get_fitness()==2);
  assert(myman3.get_fitness()==3);
  assert(myman4.get_fitness()==4);



  assert(( (myman1.get_tree() ->get_first() ->get_type()==1)&&(myman1.get_tree() ->get_first() ->get_nbr_next_nodes()==0)&&(myman1.get_tree() ->get_first() ->get_value()==2) ));
  assert(( (myman2.get_tree() ->get_first() ->get_type()==2)&&(myman2.get_tree() ->get_first() ->get_nbr_next_nodes()==0)&&(myman2.get_tree() ->get_first() ->get_value()==0) ));

  assert(( (myman3.get_tree() ->get_first() ->get_type()==0)&&(myman3.get_tree() ->get_first() ->get_nbr_next_nodes()==1)&&(myman3.get_tree() ->get_first() ->get_value()==2) ));
  assert(( (myman3.get_tree() ->get_first() ->get_next_node(0) -> get_type()==1)&&(myman3.get_tree() ->get_first() ->get_next_node(0) -> get_nbr_next_nodes()==0)&&(myman3.get_tree() ->get_first() ->get_next_node(0) -> get_value()==2) ));

  assert(( (myman4.get_tree() ->get_first() ->get_type()==0)&&(myman4.get_tree() ->get_first() ->get_nbr_next_nodes()==2)&&(myman4.get_tree() ->get_first() ->get_value()==1) ));
  assert(( (myman4.get_tree() ->get_first() ->get_next_node(0) -> get_type()==2)&&(myman4.get_tree() ->get_first() ->get_next_node(0) -> get_nbr_next_nodes()==0)&&(myman4.get_tree() ->get_first() ->get_next_node(0) -> get_value()==0) ));
  assert(( (myman4.get_tree() ->get_first() ->get_next_node(1) -> get_type()==0)&&(myman4.get_tree() ->get_first() ->get_next_node(1) -> get_nbr_next_nodes()==1)&&(myman4.get_tree() ->get_first() ->get_next_node(1) -> get_value()==2) ));
  assert(( (myman4.get_tree() ->get_first() ->get_next_node(1) -> get_next_node(0) -> get_type()==1)&&(myman4.get_tree() ->get_first() ->get_next_node(1) -> get_next_node(0) -> get_nbr_next_nodes()==0)&&(myman4.get_tree() ->get_first() ->get_next_node(1) -> get_next_node(0) -> get_value()==2) ));

}

void test_individual_fitness_compute()
{
  test_display("compute");

  Node* myvalue=new Node(1,0,2,nullptr); //X2
  Node* myconstant=new Node(2,0,0,nullptr); //0
  Node* mytab[1];
  mytab[0]= myvalue;
  Node* mynode1=new Node(0,1,2,mytab); // Not -> X2
  Node* mytab2[2] ;
  mytab2[0]=myconstant;
  mytab2[1]=mynode1;
  Node* mynode2=new Node(0,2,1,mytab2); // ou -> |0
                              //       |Not ->X2

  Node* myvalue2=new Node(1,0,0,nullptr); //X0
  Node* mytab3[2];
  mytab3[0]=myvalue2;
  mytab3[1]=mynode2;

  Node* mynode3=new Node(0,2,0,mytab3); // et -> |X0
                              //       |ou ->|0
                              //             |Not ->X2



  float data1[]={1,0,1};
  float data2[]={0,0,1};
  float data3[]={0,0,0};
  float data4[]={1,0,0};
  float data5[]={1,1,1};
  float* data[]={data1,data2,data3,data4,data5};

  float observations[]={1,1,1,1,1};


  Tree mytree1(myvalue,1);
  Tree mytree2(myconstant,1);
  Tree mytree3(mynode1,2);
  Tree mytree4(mynode2,4);
  Tree mytree5(mynode3,6);


  Individual myman1(mytree1, 1); // TTFFT
  Individual myman2(mytree2, 2); // FFFFF
  Individual myman3(mytree3, 3); // FFTTF
  Individual myman4(mytree4, 4); // FFTTF
  Individual myman5(mytree5, 5); // FFFTF



  assert(myman1.get_fitness()==1);
  assert(myman2.get_fitness()==2);
  assert(myman3.get_fitness()==3);
  assert(myman4.get_fitness()==4);
  assert(myman5.get_fitness()==5);

  myman1.fitness_compute(data,observations,5);
  myman2.fitness_compute(data,observations,5);
  myman3.fitness_compute(data,observations,5);
  myman4.fitness_compute(data,observations,5);
  myman5.fitness_compute(data,observations,5);

  assert(myman1.get_fitness()==2);
  assert(myman2.get_fitness()==5);
  assert(myman3.get_fitness()==3);
  assert(myman4.get_fitness()==3);
  assert(myman5.get_fitness()==4);



}

void test_individual_default_constructor()
{
  test_display("default constructor");
  Individual myindividual;
  cout << ( myindividual.get_fitness() == 0 )<< endl ;

}


void test_individual_copy_constructor()
{
  test_display("copy constructor");
  Node* myvalue=new Node(1,0,2,nullptr);
  Node* myconstant=new Node(2,0,0,nullptr);
  Node* mytab[1];
  mytab[0]= myvalue;
  Node* mynode1=new Node(0,1,2,mytab);
  Node* mytab2[2] ;
  mytab2[0]=myconstant;
  mytab2[1]=mynode1;
  Node* mynode2=new Node(0,2,1,mytab2);

  Tree mytree4(mynode2,4);
  
  Individual myindividual(mytree4,2);
  Individual mycopy(myindividual);
  cout<<(myindividual.get_tree()!=mycopy.get_tree())<<endl;
  cout<<(myindividual.get_fitness()==mycopy.get_fitness())<<endl;
  assert(myindividual.get_tree()->get_first()->get_type()==mycopy.get_tree()->get_first()->get_type());
  assert(myindividual.get_tree()->get_first()->get_value()==mycopy.get_tree()->get_first()->get_value());

}


void test_individual_modif_individual()
{
  test_display("Modif individual");
  Node* myvalue=new Node(1,0,2,nullptr);
  Node* myconstant=new Node(2,0,0,nullptr);
  Node* mytab[1];
  mytab[0]= myvalue;
  Node* mynode1=new Node(0,1,2,mytab);
  Node* mytab2[2] ;
  mytab2[0]=myconstant;
  mytab2[1]=mynode1;
  Node* mynode2=new Node(0,2,1,mytab2);

  Tree mytree4(mynode2,4);
  
  Individual myindividual(mytree4,2);
  myindividual.get_tree()->affichage_tree();
  myindividual.modif_individual();
  myindividual.get_tree()->affichage_tree();
} 

void test_individual_generate_daugther()
{
  test_display("Generate daugther");
  Node* myvalue=new Node(1,0,2,nullptr);
  Node* myconstant=new Node(2,0,0,nullptr);
  Node* mytab[1];
  mytab[0]= myvalue;
  Node* mynode1=new Node(0,1,2,mytab);
  Node* mytab2[2] ;
  mytab2[0]=myconstant;
  mytab2[1]=mynode1;
  Node* mynode2=new Node(0,2,1,mytab2);

  Tree mytree4(mynode2,4);
  Individual myindividual(mytree4,2);

  Individual* tab[3];
  myindividual.generate_daugther(3,tab);
  for(int i=0;i<3;i++)
  {
    tab[i]->get_tree()->affichage_tree();
  }
}

void test_individual_best_individual()
{
  test_display("Best individual");
  Node* myvalue=new Node(1,0,2,nullptr); //X2
  Node* myconstant=new Node(2,0,0,nullptr); //0
  Node* mytab[1];
  mytab[0]= myvalue;
  Node* mynode1=new Node(0,1,2,mytab); // Not -> X2
  Node* mytab2[2] ;
  mytab2[0]=myconstant;
  mytab2[1]=mynode1;
  Node* mynode2=new Node(0,2,1,mytab2); // ou -> |0
                              //       |Not ->X2

  Node* myvalue2=new Node(1,0,0,nullptr); //X0
  Node* mytab3[2];
  mytab3[0]=myvalue2;
  mytab3[1]=mynode2;

  Node* mynode3=new Node(0,2,0,mytab3); // et -> |X0
                              //       |ou ->|0
                              //             |Not ->X2



  float data1[]={1,0,1};
  float data2[]={0,0,1};
  float data3[]={0,0,0};
  float data4[]={1,0,0};
  float data5[]={1,1,1};
  float* data[]={data1,data2,data3,data4,data5};

  float observations[]={1,1,1,1,1};


  Tree mytree1(myvalue,1);
  Tree mytree2(myconstant,1);
  Tree mytree3(mynode1,2);
  Tree mytree4(mynode2,4);
  Tree mytree5(mynode3,6);


  Individual* myman1= new Individual(mytree1, 1); // TTFFT
  Individual* myman2= new Individual(mytree2, 2); // FFFFF
  Individual* myman3= new Individual(mytree3, 3); // FFTTF
  Individual* myman4= new Individual(mytree4, 4); // FFTTF
  Individual* myman5= new Individual(mytree5, 5); // FFFTF

  myman1->fitness_compute(data,observations,5);
  myman2->fitness_compute(data,observations,5);
  myman3->fitness_compute(data,observations,5);
  myman4->fitness_compute(data,observations,5);
  myman5->fitness_compute(data,observations,5);

  assert(myman1->get_fitness()==2);
  assert(myman2->get_fitness()==5);
  assert(myman3->get_fitness()==3);
  assert(myman4->get_fitness()==3);
  assert(myman5->get_fitness()==4);

  int mother1 =myman1->get_fitness();
  int mother2 =myman2->get_fitness();
  int mother3 =myman3->get_fitness();
  int mother4 =myman4->get_fitness();
  int mother5 =myman5->get_fitness();


  Individual* tab1[7];
  myman1->generate_daugther(7,tab1);
  for (int i =0 ; i<7 ; ++i)
  {
    tab1[i] ->fitness_compute(data,observations,5);
    cout << "individual  "<< i << " : "<< tab1[i]->get_fitness() <<endl;
  }
  myman1=best_individual(tab1, 7, myman1);
  assert(myman1->get_fitness() <= mother1);
  cout << "myman1 = "<< myman1->get_fitness() <<endl;

  Individual* tab2[7];
  myman2->generate_daugther(7,tab2);
  for (int i =0 ; i<7 ; ++i)
  {
    tab2[i] ->fitness_compute(data,observations,5);
    cout << "individual  "<< i << " : "<< tab2[i]->get_fitness() <<endl;

  }
  myman2=best_individual(tab2, 7, myman2);
  assert(myman2->get_fitness() <= mother2);
  cout << "myman2 = "<< myman2->get_fitness() <<endl;


  Individual* tab3[7];
  myman3->generate_daugther(7,tab3);
  for (int i =0 ; i<7 ; ++i)
  {
    tab3[i] ->fitness_compute(data,observations,5);
    cout << "individual  "<< i << " : "<< tab3[i]->get_fitness() <<endl;
  }
  myman3=best_individual(tab3, 7, myman3);
  assert(myman3->get_fitness() <= mother3);
  cout << "myman3 = "<< myman3->get_fitness() <<endl;


  Individual* tab4[7];
  myman4->generate_daugther(7,tab4);
  for (int i =0 ; i<7 ; ++i)
  {
    tab4[i] ->fitness_compute(data,observations,5);
    cout << "individual  "<< i << " : "<< tab4[i]->get_fitness() <<endl;
  }
  myman4=best_individual(tab4, 7, myman4);
  assert(myman4->get_fitness() <= mother4);
  cout << "myman4 = " << myman4->get_fitness() <<endl;



  Individual* tab5[7];
  myman5->generate_daugther(7,tab5);
  for (int i =0 ; i<7 ; ++i)
  {
    tab5[i] ->fitness_compute(data,observations,5);
    cout << "individual  "<< i << " : "<< tab5[i]->get_fitness() <<endl;
  }
  myman5=best_individual(tab5, 7, myman5);
  assert(myman5->get_fitness() <= mother5);
  cout << "myman5 = "<< myman5->get_fitness() <<endl;

}


void test_evolution_root()
{
  test_display("evolution");
  float data1[]={1,0,1,0};
  float data2[]={0,0,1,0};
  float data3[]={1,1,0,1};
  float data4[]={1,0,0,1};
  float data5[]={0,0,0,1};
  float data6[]={0,1,1,0};
  float* data[]={data1,data2,data3,data4,data5,data6};
  float obs[]={0,0,0,0,1,0};
  float fitness_tab[1000+1];
  for (int i=0;i<1000+1;++i)
  {
    fitness_tab[i]=12;
  }
  Individual* best=evolution_root(data,4, obs, 6, 300, 1000, fitness_tab);
  for (int i=0;i<best ->get_generation();++i)
  {
    cout <<"Le meilleur individu de la génération " << i << " a une fitness de : " << fitness_tab[i] << endl;
  }
  cout << "Best = "<< best->get_fitness() <<endl;
  cout << "Best = "<< best->get_tree()->get_expression()<<endl;
  
}







