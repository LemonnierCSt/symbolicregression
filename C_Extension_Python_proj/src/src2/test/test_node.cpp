#include <iostream>
#include <stdlib.h>
#include <string>

#include "../node.h"

#include "function_test_node.h"

using std::cout;
using std::endl;
using std::string;

int main()
{
  srand(time(NULL));
/*  cout<<"____________________"<<endl;
  cout<<"Test of getters"<<endl;
  Node noeud;
  cout<< "Type :" << noeud.get_type()<<endl;
  cout<< "Value :" << noeud.get_value()<<endl;
  for (int i=0;i<noeud.get_nbr_next_nodes();i++){
    cout<< "Adresse " << noeud.get_next_node(i)<<endl;
  }

  cout<<"____________________"<<endl;
  cout<<"Test of affichage_node()"<<endl;
  noeud.display();

  cout<<"____________________"<<endl;
  cout<<"Test of non_defalut constructor"<<endl;
  Node noeud11;
  Node noeud12;
  Node* tab[] = {&noeud11,&noeud12};
  Node noeud2(0,2,0,tab);
  noeud2.display();
  cout << endl;
  noeud11.display();

  cout<<"____________________"<<endl;
  cout<<"Test of default constructor with value after operators"<<endl;
  Node noeud3;
  cout<< "Type :" << noeud3.get_type()<<endl;
  cout<< "Value :" << noeud3.get_value()<<endl;
  if (noeud3.get_type() == 0){
    cout<<"Different next_nodes"<<endl;
    for (int i=0;i<noeud3.get_nbr_next_nodes();i++){
      cout<< "Adresse " << noeud3.get_next_node(i)<<endl;
    }
  }
  */
  test_node_data_length();

  for (int i=0;i<3;++i)
  {
    test_node_default_constructor();

  }

  test_operation_result_X();
  test_operation_result_XY();

  test_node_value_constructor();

  test_node_compute();

  test_modif_values();
  
  cout<<"____________________"<<endl;
  cout<<"Test of parcours"<<endl;
  test_parcours();
  return 0;

}
