#ifndef FUNCTION_TEST_TREE_H
#define FUNCTION_TEST_TREE_H

void test_tree_data_length(int data_length=3);


void test_tree_copy_constructor();

void test_tree_get_value();

void  test_tree_value_constructor();

void test_tree_addition();

void test_tree_deletion();

void test_tree_mutation();

void test_tree_access_node();

void test_get_expression();

void test_tree_default_constructor();

#endif // FUNCTION_TEST_TREE_H
