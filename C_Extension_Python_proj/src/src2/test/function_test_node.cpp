#include <iostream>
#include <stdlib.h>
#include <string>

#include "../node.h"

#include "function_test_node.h"

using std::cout;
using std::endl;
using std::string;

void test_display(string test_name) // header of a serial of test
{
  cout<<"____________________"<<endl;
  cout<<"Test of "<< test_name <<endl;
}

void assert(bool condition)
{
  cout << condition << endl;
}

void test_node_data_length(int data_length)
{
  test_display("data_length_node");
  Node mynode;
  Node mynode2;
  cout<<(mynode.data_length()==data_length)<<endl;
  cout<<(mynode2.data_length()==data_length)<<endl;
}

void test_operation_result_X(){
  test_display("test_operation_result_X");
  cout<<"  And operator"<<endl;
  Node noeud41(0,0,1,nullptr);
  Node noeud42(0,0,1,nullptr);
  Node noeud43(0,0,0,nullptr);
  Node* tab2[] = {&noeud41,&noeud42};
  Node noeud4(1,2,0,tab2);
  cout << (noeud4.operation_result(noeud41.get_value(),noeud42.get_value()) == true) << endl;
  tab2[1] = &noeud43;
  cout << (noeud4.operation_result(noeud41.get_value(),noeud43.get_value()) == false) << endl;

  cout<<"  Or operator"<<endl;
  Node noeud5(1,2,1,tab2);
  cout << (noeud5.operation_result(noeud41.get_value(),noeud43.get_value()) == true) << endl;
  tab2[0] = &noeud43;
  cout << (noeud5.operation_result(noeud43.get_value(),noeud43.get_value()) == false) << endl;
}

void test_operation_result_XY(){
  test_display("test_operation_result_XY");
  Node noeud41(0,0,1,nullptr);
  Node noeud42(0,0,1,nullptr);
  Node noeud43(0,0,0,nullptr);
  Node* tab3[] = {&noeud41};
  Node noeud6(1,1,2,tab3);
  cout << (noeud6.operation_result(noeud41.get_value()) == false) << endl;
  tab3[0] = &noeud43;
  cout << (noeud6.operation_result(noeud43.get_value()) == true) << endl;
}

void test_node_default_constructor()
{
  test_display("default_constructor_node");

  Node mynode;
  cout<<((mynode.get_type()==0)||(mynode.get_type()==1)||(mynode.get_type()==2))<<endl;
  if (mynode.get_type()==0)
  {
    cout<<((mynode.get_value()==0)||(mynode.get_value()==1)||(mynode.get_value()==2))<<endl;
    if (mynode.get_value()==2)
    {
      cout<<(mynode.get_nbr_next_nodes()==1)<<endl;
    }
    else
    {
      cout<<(mynode.get_nbr_next_nodes()==2)<<endl;
    }
    for (int i=0; i<mynode.get_nbr_next_nodes();i++)
    {
      cout << (mynode.get_next_node(i)== nullptr) << endl;
    }
  }
  else if (mynode.get_type()==1) //variable
  {
    assert(mynode.get_nbr_next_nodes()==0);
    assert(mynode.get_value()<=mynode.data_length());
  }
  else //constant
  {
    assert(mynode.get_nbr_next_nodes()==0);
  }

}

void test_node_value_constructor()
{
  test_display("Test of node_value_constructor");

  Node myvalue(1,0,2,nullptr); //X2
  Node myconstant(2,0,0,nullptr); //0
  Node* mytab[1];
  mytab[0]= &myvalue;
  Node mynode1(0,1,2,mytab); // Not -> X2
  Node* mytab2[2] ;
  mytab2[0]=&myconstant;
  mytab2[1]=&mynode1;
  Node mynode2(0,2,1,mytab2); // ou -> |0
                              //       |Not ->X2

  assert(( (myvalue.get_type()==1)&&(myvalue.get_nbr_next_nodes()==0)&&(myvalue.get_value()==2) ));
  assert(( (myconstant.get_type()==2)&&(myconstant.get_nbr_next_nodes()==0)&&(myconstant.get_value()==0) ));

  assert(( (mynode1.get_type()==0)&&(mynode1.get_nbr_next_nodes()==1)&&(mynode1.get_value()==2) ));
  assert(( (mynode1.get_next_node(0) -> get_type()==1)&&(mynode1.get_next_node(0) -> get_nbr_next_nodes()==0)&&(mynode1.get_next_node(0) -> get_value()==2) ));

  assert(( (mynode2.get_type()==0)&&(mynode2.get_nbr_next_nodes()==2)&&(mynode2.get_value()==1) ));
  assert(( (mynode2.get_next_node(0) -> get_type()==2)&&(mynode2.get_next_node(0) -> get_nbr_next_nodes()==0)&&(mynode2.get_next_node(0) -> get_value()==0) ));
  assert(( (mynode2.get_next_node(1) -> get_type()==0)&&(mynode2.get_next_node(1) -> get_nbr_next_nodes()==1)&&(mynode2.get_next_node(1) -> get_value()==2) ));
  assert(( (mynode2.get_next_node(1) -> get_next_node(0) -> get_type()==1)&&(mynode2.get_next_node(1) -> get_next_node(0) -> get_nbr_next_nodes()==0)&&(mynode2.get_next_node(1) -> get_next_node(0) -> get_value()==2) ));

}




void test_node_compute()
{
  test_display("Test of compute");

  Node myvalue(1,0,2,nullptr); //X2
  Node myconstant(2,0,0,nullptr); //0
  Node* mytab[1];
  mytab[0]= &myvalue;
  Node mynode1(0,1,2,mytab); // Not -> X2
  Node* mytab2[2] ;
  mytab2[0]=&myconstant;
  mytab2[1]=&mynode1;
  Node mynode2(0,2,1,mytab2); // ou -> |0
                              //       |Not ->X2

  float data[]={1,0,1,0,1};

  assert(myvalue.compute(data)==1);
  assert(myconstant.compute(data)==0);
  assert(mynode1.compute(data)==0);
  assert(mynode2.compute(data)==0);
}

void test_modif_values()
{
  test_display("Test of modif_values");

  Node n21(0,0,1,nullptr);
  Node n22(0,0,1,nullptr);
  Node* tab[] = {&n21,&n22};
  Node n1(0,2,1,tab);
  n1.modif_values(1);
  assert(n1.get_type() == 1);
  assert(n1.get_value() == 0|| n1.get_value() == 1);
  assert(n1.get_nbr_next_nodes() == 0);
  assert(n1.get_next_node(0) == nullptr);

  Node n2(1,0,1,nullptr);
  n2.modif_values(0);
  assert(n2.get_type() == 0);
  assert(n2.get_value() == 0 || n2.get_value() == 1 || n2.get_value() == 2);
  assert(n2.get_nbr_next_nodes() == 1 || n2.get_nbr_next_nodes() == 2);
  assert(n2.get_next_node(0) == nullptr);
  if (n2.get_nbr_next_nodes() == 2)
      assert(n2.get_next_node(1) == nullptr);
}

void test_parcours()
{
  Node noeud11(1,0,1,nullptr);
  Node noeud12(1,0,0,nullptr);
  Node *tab[] = {&noeud11};
  Node noeud21(0,1,2,tab);
  Node *tab2[] = {&noeud12,&noeud21};
  Node noeud2(0,2,0,tab2);

  Node* node[4];                //Déclare un tableau d'adresse de noeud de taille équivalent a la taille de l'arbre
  node[0]=&noeud2;          //Met en case 0 le premier noeud de l'arbre
  int depart=1;                 //Met le point de départ à 0
  noeud2.parcours(node,&depart);  //Appelle la fonction parcours de noeud
  cout<< (node[0]==&noeud2) <<endl;
  cout<< (node[1]==&noeud12) <<endl;
  cout<< (node[2]==&noeud21) <<endl;
  cout<< (node[3]==&noeud11) <<endl;
}


