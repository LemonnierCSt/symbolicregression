#ifndef FUNCTION_TEST_INDIVIDUAL_H
#define FUNCTION_TEST_INDIVIDUAL_H

void test_individual_data_length();
void test_individual_value_constructor();
void test_individual_fitness_compute();
void test_individual_default_constructor();
void test_individual_copy_constructor();
void test_individual_modif_individual();
void test_individual_generate_daugther();
void test_individual_best_individual();
void test_evolution_root();
#endif // FUNCTION_TEST_INDIVIDUAL_H
