#ifndef FUNCTION_TEST_NODE_H
#define FUNCTION_TEST_NODE_H

#include <string>
#include <iostream>

using std::cout;
using std::endl;
using std::string;

void test_display(string test_name); // header of a serial of test

void assert(bool condition); //display 0 if false


void test_node_default_constructor();

void test_node_value_constructor();

void test_node_data_length(int data_length=3);

void test_operation_result_X();
void test_operation_result_XY();

void test_node_compute();

void test_modif_values();

void test_parcours();

#endif // FUNCTION_TEST_NODE_H
