#include <iostream>
#include <stdlib.h>
#include <string>

#include "../node.h"
#include "../tree.h"

#include "function_test_tree.h"
#include "function_test_node.h"

using std::cout;
using std::endl;
using std::string;

void  test_tree_value_constructor()
{
  test_display("tree_value_constructor");

  Node* myvalue= new Node(1,0,2,nullptr);
  Node* myconstant= new Node(2,0,0,nullptr);
  Node* mytab[1];
  mytab[0]= myvalue;
  Node* mynode1= new Node(0,1,2,mytab);
  Node* mytab2[2] ;
  mytab2[0]=myconstant;
  mytab2[1]=mynode1;
  Node* mynode2= new Node(0,2,1,mytab2);

  Tree mytree1(myvalue,1);
  Tree mytree2(myconstant,1);
  Tree mytree3(mynode1,2);
  Tree mytree4(mynode2,4);

  assert(( (mytree1.get_first() ->get_type()==1)&&(mytree1.get_first() ->get_nbr_next_nodes()==0)&&(mytree1.get_first() ->get_value()==2) ));
  assert(( (mytree2.get_first() ->get_type()==2)&&(mytree2.get_first() ->get_nbr_next_nodes()==0)&&(mytree2.get_first() ->get_value()==0) ));

  assert(( (mytree3.get_first() ->get_type()==0)&&(mytree3.get_first() ->get_nbr_next_nodes()==1)&&(mytree3.get_first() ->get_value()==2) ));
  assert(( (mytree3.get_first() ->get_next_node(0) -> get_type()==1)&&(mytree3.get_first() ->get_next_node(0) -> get_nbr_next_nodes()==0)&&(mytree3.get_first() ->get_next_node(0) -> get_value()==2) ));

  assert(( (mytree4.get_first() ->get_type()==0)&&(mytree4.get_first() ->get_nbr_next_nodes()==2)&&(mytree4.get_first() ->get_value()==1) ));
  assert(( (mytree4.get_first() ->get_next_node(0) -> get_type()==2)&&(mytree4.get_first() ->get_next_node(0) -> get_nbr_next_nodes()==0)&&(mytree4.get_first() ->get_next_node(0) -> get_value()==0) ));
  assert(( (mytree4.get_first() ->get_next_node(1) -> get_type()==0)&&(mytree4.get_first() ->get_next_node(1) -> get_nbr_next_nodes()==1)&&(mytree4.get_first() ->get_next_node(1) -> get_value()==2) ));
  assert(( (mytree4.get_first() ->get_next_node(1) -> get_next_node(0) -> get_type()==1)&&(mytree4.get_first() ->get_next_node(1) -> get_next_node(0) -> get_nbr_next_nodes()==0)&&(mytree4.get_first() ->get_next_node(1) -> get_next_node(0) -> get_value()==2) ));

}

void test_tree_data_length(int data_length)
{
  test_display("data_length_tree");

  Tree mytree;
  Tree mytree2;
  cout<<(mytree.data_length()==data_length)<<endl;
  cout<<(mytree2.data_length()==data_length)<<endl;
}

void test_tree_copy_constructor(){
  test_display("tree_copy_constructor");
  
  Node* noeud11= new Node(2,0,1,nullptr);
  Node* noeud12= new Node(2,0,0,nullptr);
  Node *tab[] = {noeud11};
  Node* noeud21= new Node(0,1,2,tab);
  Node *tab2[] = {noeud12,noeud21};
  Node* noeud2= new Node(0,2,0,tab2);
  Tree arbre(noeud2,4);
//    Tree mycopy(arbre); 
/*  assert(arbre.get_length() == mycopy.get_length());
  assert(arbre.get_first() != mycopy.get_first());
  assert(arbre.get_first()->get_nbr_next_nodes() == mycopy.get_first()->get_nbr_next_nodes());
  assert(arbre.get_first()->get_type()==mycopy.get_first()->get_type());
  assert(arbre.get_first()->get_value()==mycopy.get_first()->get_value());
  for (int k = 1; k < arbre.get_length(); k++){
        assert( (arbre.access_node(k)->get_nbr_next_nodes()==mycopy.access_node(k)->get_nbr_next_nodes()) && (arbre.access_node(k)->get_type()==mycopy.access_node(k)->get_type()) && (arbre.access_node(k)->get_value()==mycopy.access_node(k)->get_value()));
    }
  // next_nodes
  assert(arbre.get_first()->get_next_node(0) == arbre.access_node(1));
  assert(arbre.access_node(2)->get_next_node(0) == arbre.access_node(3));

  // independant copy
  noeud11->modif_values(1);
  assert(arbre.access_node(3)->get_type() != mycopy.access_node(3)->get_type());*/
} 


void test_tree_get_value()
{
  test_display("get value");



  Node* myvalue= new Node(1,0,2,nullptr); //X2
  Node* myconstant= new Node(2,0,0,nullptr); //0
  Node* mytab[1];
  mytab[0]= myvalue;
  Node* mynode1= new Node(0,1,2,mytab); // Not -> X2
  Node* mytab2[2] ;
  mytab2[0]=myconstant;
  mytab2[1]=mynode1;
  Node* mynode2= new Node(0,2,1,mytab2); // ou -> |0
                              //       |Not ->X2

  float data[]={1,0,1,0,1};
  Tree mytree1(myvalue,1);
  Tree mytree2(myconstant,1);
  Tree mytree3(mynode1,2);
  Tree mytree4(mynode2,4);


  assert(mytree1.get_value(data)==1);
  assert(mytree2.get_value(data)==0);
  assert(mytree3.get_value(data)==0);
  assert(mytree4.get_value(data)==0);
}


void test_tree_addition()
{
  test_display("addition");
  Node* noeud11= new Node(1,0,1,nullptr);
  Node* noeud12= new Node(1,0,0,nullptr);
  Node *tab[] = {noeud11};
  Node* noeud21= new Node(0,1,2,tab);
  Node *tab2[] = {noeud12,noeud21};
  Node* noeud2= new Node(0,2,0,tab2);
  Tree arbre(noeud2,3);
  arbre.addition();
  arbre.addition();
  arbre.addition();
  arbre.deletion();
  arbre.affichage_tree();
}

void test_tree_deletion()
{
  test_display("deletion");
  Node* noeud11= new Node(1,0,1,nullptr);
  Node* noeud12= new Node(1,0,0,nullptr);
  Node *tab[] = {noeud11};
  Node* noeud21= new Node(0,1,2,tab);
  Node *tab2[] = {noeud12,noeud21};
  Node* noeud2= new Node(0,2,0,tab2);
  Tree arbre(noeud2,3);
  arbre.addition();
  arbre.addition();
  arbre.deletion();
  arbre.deletion();
  arbre.deletion();
  arbre.affichage_tree();
}

void test_tree_mutation()
{
  test_display("mutation");
  Node* noeud11= new Node(1,0,1,nullptr);
  Node* noeud12= new Node(1,0,0,nullptr);
  Node *tab[] = {noeud11};
  Node* noeud21= new Node(0,1,2,tab);
  Node *tab2[] = {noeud12,noeud21};
  Node* noeud2= new Node(0,2,0,tab2);
  Tree arbre(noeud2,3);
  arbre.mutation();
  arbre.affichage_tree();
}

void test_tree_access_node()
{
  test_display("access node");
  Node* noeud11= new Node(1,0,1,nullptr);
  Node* noeud12= new Node(1,0,0,nullptr);
  Node *tab[] = {noeud11};
  Node* noeud21= new Node(0,1,2,tab);
  Node *tab2[] = {noeud12,noeud21};
  Node* noeud2= new Node(0,2,0,tab2);
  Tree arbre(noeud2,3);
  
  Node* Jnode = arbre.access_node(2);
  cout <<(Jnode==noeud21) <<endl;
}

void test_get_expression()
{
  test_display("get_expression");
  // test AND / NOT / VALUE
  Node* nM11= new Node(1,0,1,nullptr);
  Node* nM12= new Node(1,0,0,nullptr);
  Node *tabM1[] = {nM11};
  Node* nM21= new Node(0,1,2,tabM1);
  Node *tabM2[] = {nM12,nM21};
  Node* nM1= new Node(0,2,0,tabM2);
  Tree M1(nM1,3);
  assert(M1.get_expression() == ("(X0 AND (NOT(X1)))"));

  // test OR
  Node* nM2= new Node(0,2,1,tabM2);
  Tree M2(nM2,3);
  assert(M2.get_expression() == ("(X0 OR (NOT(X1)))"));
}

void test_tree_default_constructor()
{
  test_display("default_constructor");
  Tree tree;
  tree.affichage_tree();
}



