#include <iostream>
#include <stdlib.h>
#include <string>

#include "../node.h"
#include "../tree.h"

#include "function_test_tree.h"
#include "function_test_node.h"

using std::cout;
using std::endl;
using std::string;

int main()
{
  srand(time(NULL));

  test_tree_data_length();

  test_tree_value_constructor();
  test_tree_get_value();
  test_tree_default_constructor();

  test_tree_copy_constructor();
/*
  //  Node noeud;

  cout<< "Operateur :" << noeud.get_is_operator()<<endl;
  cout<< "Value :" << noeud.get_value()<<endl;
  for (int i=0;i<noeud.get_nbr_next_nodes();i++)
    cout<< "Adresse " << noeud.get_next_node(i)<<endl;
*/

//  noeud.affichage_node();


 test_tree_access_node();

  test_tree_addition();

  test_tree_deletion();

  test_tree_mutation();

/*  Node* noeud3;
  noeud3=arbre.get_first();

  noeud3->affichage_node();*/


 test_tree_get_value();

 test_get_expression();

  return 0;
}
