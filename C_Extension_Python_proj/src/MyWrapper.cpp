#include <Python.h>//include the "Python.h" header before any other include
#include<iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string>
#include <math.h>
#include <zlib.h>
#include <errno.h>
#include "def_PyC.h"

#include "individual.h"
#include "node.h"
#include "tree.h"

// Name for the cpp object "capsules"

#define NAME_CAPSULE_INDIVIDUAL "Model"
#define NAME_CAPSULE_NODE "Node"
#define NAME_CAPSULE_TREE "Tree"


////////////////////////////////////////////
static Individual* IndividualPythonToC(PyObject* args){
        Individual* my_Individual;
        PyObject* capsule;
        if (!PyArg_ParseTuple(args, "O", &capsule)){
                return NULL;
        }
        my_Individual = (Individual*) PyCapsule_GetPointer(capsule,NAME_CAPSULE_INDIVIDUAL);
        return my_Individual;
}

void INDIVIDUALCapsuleDestructor(PyObject* capsule){
        Individual* my_Individual = (Individual*) PyCapsule_GetPointer(capsule,NAME_CAPSULE_INDIVIDUAL);
  delete my_Individual;
}
/////////////////////////




//////////////////////////////////////////
//////////////////////////////////////////
static PyObject*  GetFitness(PyObject* self, PyObject* args){
    Individual*  my_Individual = IndividualPythonToC(args);//to do
    float fitness= my_Individual->get_fitness();
    return Py_BuildValue("f",fitness);//transforme int c++ en int python
}

static PyObject*  Demo(PyObject* self){
    Individual*  my_Demo_Individual = demo();//demo to do in c++ problème
    PyObject* capsule = PyCapsule_New(my_Demo_Individual, NAME_CAPSULE_INDIVIDUAL, INDIVIDUALCapsuleDestructor);
    return capsule;
}
static PyObject*  View(PyObject* self, PyObject* args){
    Individual*  my_Individual = IndividualPythonToC(args);//decapsule
    std::cout<<"Model's expression:"<<my_Individual->get_tree()->get_expression()<<std::endl;

    Py_INCREF(Py_None);
    return Py_None;
}

//////////////////////////////////////////
//////////////////////////////////////////





/////////////////////
static PyObject* IndividualTranslator(PyObject* self, PyObject* args){
        PyListObject* list_data;
        PyListObject* list_observations;
        int nbr_daughters;
        int nbr_generations;
  // parse the argument tuple: https://docs.python.org/3/c-api/arg.html
        if (!PyArg_ParseTuple(args, "OOhi", &list_data, &list_observations,&nbr_generations,&nbr_daughters)){//make python lists readeable by c++
                return NULL;
        }
        int size_data= PyList_Size((PyObject*) list_data);//5
        int size_observations= PyList_Size((PyObject*) list_observations);//5
        
        //
        PyListObject* sublist0= (PyListObject*) PyList_GetItem((PyObject*) list_data, (Py_ssize_t) 0);
        int size_data_vectors=PyList_Size((PyObject*) sublist0);//3
        
        float** data= new float* [size_data];//[size_data_vectors];//creation of the c++ 2D arrays//"long//float data[size_data][size_data_vectors];
        
        
        for(int i =0;i< size_data;i++){
            data[i]=new float[size_data_vectors];
        }
        float observations[size_observations];//"long observations[size_observations];

        for (int i = 0; i < size_observations; i++){
          //PyLongObject* item=(PyLongObject*) PyList_GetItem( (PyObject*) list_observations, (Py_ssize_t) i);
          //long num = PyLong_AsLong((PyObject*)item);
          float num= (float) PyFloat_AsDouble(PyList_GetItem( (PyObject*) list_observations , (Py_ssize_t) i));// check PyFloat_AsDouble for doubles //int num= (int) PyLong_AsLong(PyList_GetItem( (PyObject*) list_observations , (Py_ssize_t) i));
          observations[i]= num;
          //printf("%d\n",(int)num);
        }


        for (int i = 0; i < size_data; i++){
          PyListObject* sublist= (PyListObject*) PyList_GetItem((PyObject*) list_data, (Py_ssize_t) i);//get sublist of observations
          size_data_vectors=PyList_Size((PyObject*) sublist);//get size of sublist(nb of variables explicatives)

            for (int j=0; j<size_data_vectors;j++){
              //PyLongObject* item=(PyLongObject*) PyList_GetItem( (PyObject*) sublist, (Py_ssize_t) i);//get values_vectors in the current sublist
              //long num = PyLong_AsLong((PyObject*)item);

              float num=(float)PyFloat_AsDouble(PyList_GetItem( (PyObject*) sublist , (Py_ssize_t) j));//int num=(int)PyLong_AsLong(PyList_GetItem( (PyObject*) sublist , (Py_ssize_t) j));
              data[i][j]= num;
              //printf("%d\n",(int)data[i][j]);
            }
        }
        



        float fitness_tab[nbr_generations+1];
        Individual* my_model= evolution_root(data,size_data_vectors,observations,size_observations,nbr_generations,nbr_daughters,fitness_tab);//size data ou size data vector

        PyObject* capsule = PyCapsule_New(my_model, NAME_CAPSULE_INDIVIDUAL, INDIVIDUALCapsuleDestructor);///TO DO
        //SUPPRESSION
        for (int i = 0; i< size_data; i++)
           {
              delete [] data[i];
           }
        delete [] data;
        return capsule;
}
///////////////////////////




// Module functions {<python function name>, <function in wrapper>, <parameters flag>, <doctring>}
// https://docs.python.org/3/c-api/structures.html
static PyMethodDef module_funcs[] = {
    {"generate_model", (PyCFunction)IndividualTranslator, METH_VARARGS, "Create an instance of class Individual\n\nArgs:\n\t data (list of lists): list containing the explicatives values\n\t observations (list) : list containing the observed values\n\t nbr_daugthers (int) : number of daughters at each generation (Default value=10)\n\t nbr_iterations (int) : number of generations created (Default value=200) \n\nReturns:\n\t capsule: Object Individual capsule"},
    {"get_fitness",(PyCFunction)GetFitness,METH_VARARGS,"Returns the fitness of the model\n\n Args: capsuleModel(Capsule): object Model capsule"},
    {"demo",(PyCFunction)Demo,METH_NOARGS,"Returns a demo-model: the explanatory variables used where: the vectors of data X0 {0,1,0} X1{0,1,1} X2{0,0,1} X3{1,0,0} X4{1,1,0}. The vector of observed values used was Y(0,1,1,0,1).The demo model is '(X0 AND X1) OR X2' and has a fitness of 0.833. "},
     {"view",(PyCFunction)View,METH_VARARGS,"displays the model in fonction form(\n\n Args: capsuleModel(Capsule): object Model capsule"},
                {NULL, NULL, METH_NOARGS, NULL}


};//changer ATTENTION CAR C'EST pas lke cster par deafaut qu'on veut, c'est le csterur par données la c'estpoour terter le wrapper

static struct PyModuleDef moduledef = {
        PyModuleDef_HEAD_INIT,
        "symbolic_regression",
        "symbolic_regression module is a C++ extension for python which performs symbolic regression on boolean data sets",
        sizeof(PyObject*),
                                module_funcs,
                                NULL,
        NULL,
        NULL,
        NULL
};

PyMODINIT_FUNC PyInit_symbolic_regression(void){
    PyObject* module = PyModule_Create(&moduledef);
                return module;
}
