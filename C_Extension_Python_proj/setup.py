import os
os.environ["CC"] = "c++"
from distutils.core import setup, Extension
module = Extension('symbolic_regression', ["src/MyWrapper.cpp","src/src2/node.cpp","src/src2/individual.cpp","src/src2/tree.cpp"],include_dirs=["src","src/src2"],libraries=[])
module.extra_compile_args = []#,'-pg']

setup(name='symbolic_regression',
	  version='1.0',
	  author='Claire Lemonnier',
	  author_email='claire.lemonnier@insa-lyon.fr',
	  ext_modules=[module])
	  
	  
