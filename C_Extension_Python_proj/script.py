import symbolic_regression as sr

print("Test régression symbolique:")

data=[]

data.append([1,0,1])
data.append([0,0,1])
data.append([1,1,0])
data.append([1,0,0])
data.append([0,0,0])
data.append([0,1,1])
observations=[1,0,0,1,0,0]

nbr_iterations=80
nbr_daugthers=20
ind1=sr.generate_model(data,observations, nbr_iterations,nbr_daugthers)
print("fitness:",sr.get_fitness(ind1))
sr.view(ind1)


