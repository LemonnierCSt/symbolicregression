import symbolic_regression as sr

print("test regression symbolique")

data=[]

data.append([0,0,0,0,1])
data.append([0,0,1,0,0])
data.append([1,1,0,1,1])
data.append([1,1,0,1,0])
data.append([1,0,1,0,0])
data.append([1,1,0,0,1])
data.append([0,0,1,1,0])
data.append([0,1,1,1,0])

observations=[0,0,1,1,1,0,0,1]

nbr_iterations=100
nbr_daugthers=100
ind1=sr.generate_model(data,observations, nbr_iterations,nbr_daugthers)
print("fitness:",sr.get_fitness(ind1))
sr.view(ind1)


